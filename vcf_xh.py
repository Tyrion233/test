# -*- coding: utf-8 -*-
"""
Created on Tue Jul  3 22:29:38 2018

@author: HP
"""

import sys

file = sys.argv[1]

with open(file,'r') as fi:
    file_content = fi.readlines()
    for line in file_content:
        if not line.startswith('#'):
            if line.split('\t')[0]=='chrY' and line.split('\t')[6]=='PASS' and int(line.split(':')[6])>100:
                print('chrY Begin and Contain : \n {0}'.format(line))
                

            